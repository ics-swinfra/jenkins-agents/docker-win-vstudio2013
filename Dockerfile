FROM microsoft/windowsservercore

#Install chocolatery
RUN @powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

# Installation of Docker and GIT by Choco
RUN choco install -y docker
RUN choco install -y git

# Installation of Visual Studio 2013
RUN mkdir c:\\Visual_Studio_Pro_2013
COPY vs_professional.exe /Visual_Studio_Pro_2013/
COPY AdminDeployment.xml /Visual_Studio_Pro_2013/

### NOTE! According to help of excape dockerfile command, escaping is NOT done in the RUN
SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

RUN     Start-Process "c:\Visual_Studio_Pro_2013\vs_professional.exe" \
        -ArgumentList '/AdminFile c:\Visual_Studio_Pro_2013\AdminDeployment.xml  /Q /NoRestart /L MyVSInstallLog' \
        -Wait;  \
        echo 'We setup the >> operator for proper encoding ...' ; \
        $PSDefaultParameterValues['Out-File:Encoding']='ascii'; \             
        $ErrorActionPreference='Continue';\
                
         try { \            
            Remove-Item -Force -Recurse 'C:\Visual_Studio_Pro_2013' ; \           
            Remove-Item -Force 'C:\MyVSInstallLog*' ; \
            Remove-Item -Force 'C:\Windows\Installer\*.msi' ; \
            Remove-Item -Recurse -Force 'C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\VCWizards' ; \
            Remove-Item -Recurse -Force 'C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC#\' ; \
            Remove-Item -Recurse -Force 'C:\Program Files (x86)\Microsoft Visual Studio 12.0\FSharp\' ; \
            Remove-Item  -Force 'C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\atlmfc\lib\*.pdb' ; \
            Remove-Item  -Force 'C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\atlmfc\lib\*.lib' ; \
            Remove-Item  -Force 'C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\atlmfc\lib\*.dll' ; \
            Remove-Item -Recurse -Force 'C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\redist\GraphicsDbgRedist\' ; \
            Remove-Item -Recurse -Force 'C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\amd64_arm\' ; \
            Remove-Item -Recurse -Force 'C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\amd64_x86\' ; \
            Remove-Item -Recurse -Force 'C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\arm\' ; \
        } catch {} \
        
        Clear-RecycleBin -Confirm:$false 

CMD [ "cmd" ]